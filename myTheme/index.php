<?php
/**
 * The main template file.
 *
 */

get_header(); ?>

<div class="about-us">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<h2>About us</h2>
				<p class="italic">Our Short Story</p>
			</div>
			<div class="col-lg-7">
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				<button>Read More</button>
			</div>
		</div>
	</div>
</div>

<div class="services">
	<div class="container">
		<h2>Services</h2>
		<small class="italic">What we are doing</small>
		<ul class="list-unstyled row">
			<li class="col-lg-6">
				<p class="header-p text-uppercase business">Boosting your business</p>
				<p>Lorem Ipsum is simply dummy text of the printing and industry
					Lorem Ipsum has been the industry's standard dummy text.</p>
			</li>
			<li class="col-lg-6">
				<p class="header-p text-uppercase support">Online support</p>
				<p>Lorem Ipsum is simply dummy text of the printing and industry
					Lorem Ipsum has been the industry's standard dummy text.</p>
			</li>
			<li class="col-lg-6">
				<p class="header-p text-uppercase analizing">analyzing business strategy</p>
				<p>Lorem Ipsum is simply dummy text of the printing and industry
					Lorem Ipsum has been the industry's standard dummy text.</p>
			</li>
			<li class="col-lg-6">
				<p class="header-p text-uppercase management">Time managment</p>
				<p>Lorem Ipsum is simply dummy text of the printing and industry
					Lorem Ipsum has been the industry's standard dummy text.</p>
			</li>
		</ul>
		<button>View more</button>
	</div>
</div>

<div class="carousel-wrap">
	<div class="container">
		<h2>Clients</h2>
		<p class="italic">Whats our client says</p>
		<div id="carousel-example-generic" class="carousel row slide" data-ride="carousel">

			<!-- Wrapper for slides -->
			<div class="carousel-inner col-xs-12">
				<div class="item active">
					<div class="all-clients row">
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-1.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-2.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-3.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="item">
					<div class="all-clients row">
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-1.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-2.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-3.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="item">
					<div class="all-clients row">
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-1.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-2.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
						<div class="client col-lg-4">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown.</p>
							<div class="description">
								<p class="col-lg-5"><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/face-3.png" alt="face"></p>
								<div class="col-lg-7">
									<p class="name">Krishnan Unni</p>
									<p>Designation</p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- Indicators -->
				<ol class="carousel-indicators pull-left">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
			</div>

		</div>
	</div>

</div>

<div class="news">
	<div class="container">
		<h2>News</h2>
		<p class="italic">From Our Blog</p>
		<div class="row">
			<div class="col-lg-5">
				<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/picture.png" alt="picture"></p>
				<h4>Blog Heading</h4>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy...</p>
			</div>
			<div class="col-lg-6">
				<section>
					<h4>Blog Heading</h4>
					<p class="date">30 - Oct - 2015</p>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy...</p>
				</section>
				<section>
					<h4>Blog Heading</h4>
					<p class="date">1 - Nov - 2015</p>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled...</p>
					<button>View more</button>
				</section>
			</div>
		</div>
	</div>
</div>

<div class="partners">
	<div class="container">
		<h2>Partners</h2>
		<p class="italic">Our Great Partners</p>
		<div id="carousel-example-generic" class="carousel row slide" data-ride="carousel">

			<!-- Wrapper for slides -->
			<div class="carousel-inner col-xs-12 center-xs">
				<div class="item active row">
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-6.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-5.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-4.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-3.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-2.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-1.png" alt="partner"></p>
					</div>
				</div>

				<div class="item row">
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-6.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-5.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-4.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-3.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-2.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-1.png" alt="partner"></p>
					</div>
				</div>

				<div class="item row">
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-6.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-5.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-4.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-3.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-2.png" alt="partner"></p>
					</div>
					<div class="col-lg-2">
						<p><img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/vector-1.png" alt="partner"></p>
					</div>
				</div>
				<!-- Indicators -->
				<ol class="carousel-indicators center-xs">
					<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic" data-slide-to="1"></li>
					<li data-target="#carousel-example-generic" data-slide-to="2"></li>
				</ol>
			</div>

		</div>
	</div>
</div>


<?php
get_footer();
