<?php
    get_header();
?>

<?php
/* Template Name: My Custom Page */
?>



<?php
$current_page = (get_query_var('paged')) ? get_query_var('paged') : 1; // определяем текущую страницу блога
$args = array(
    'posts_per_page' => get_option('posts_per_page'), // значение по умолчанию берётся из настроек, но вы можете использовать и собственное
    'paged'          => $current_page // текущая страница
);
query_posts( $args );

$wp_query->is_archive = true;
$wp_query->is_home = false;

?>


<div class="container posts">
    <?php if(have_posts()):
        while(have_posts()): the_post(); ?>
            <section class="row">
                <p class="col-lg-1"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '60');?></p>
                <div class="post col-lg-11">
                    <?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
                    <p class="date text-uppercase">Posted by: <a href="#" class="blog-admin"><?php the_author() ;?></a>, on <?php echo get_the_date('F - d - Y'); ?></p>
                    <p class="center-xs"><?php if ( has_post_thumbnail() ) {
                            the_post_thumbnail('full');
                        } ?></p>

                    <div class="row between-xs">
                        <?php the_content('Read more') ?>
                        <?php echo do_shortcode('[supsystic-social-sharing id="1"]') ?>
                    </div>
                </div>
            </section>
        <?php endwhile;

    else:
        echo '<p>No content found </p>';
    endif; ?>
    <nav id="<?php echo $html_id; ?>" class="navigation center-xs" role="navigation" >
        <?php if ( function_exists( 'wp_pagenavi' ) ) wp_pagenavi(); ?>
    </nav>
</div>

<?php
get_footer();
