<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="main" class="site-main container" role="main">

		<?php
		while ( have_posts() ) : the_post(); ?>

		<section class="row">
                <p class="col-lg-1"><?php $author_email = get_the_author_email(); echo get_avatar($author_email, '60');?></p>
			<div class="post col-lg-11">
				<?php the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
				<p class="date text-uppercase">Posted by: <a href="#" class="blog-admin"><?php the_author() ;?></a>, on <?php echo get_the_date('F - d - Y'); ?></p>
				<p class="center-xs"><?php if ( has_post_thumbnail() ) {
						the_post_thumbnail('full');
					} ?></p>

				<?php the_content('Read more') ?>
				<?php echo do_shortcode('Share: [supsystic-social-sharing id="1"]') ?>
			</div>
			</section>

			<?php // If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</div><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
