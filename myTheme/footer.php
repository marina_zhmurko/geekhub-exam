<?php
/**
 * The template for displaying the footer.
 */

?>

	</div><!-- #content -->

	<footer class="site-footer" role="contentinfo">
		<div class="container">
			<div class="col-lg-4">
				<img src="http://localhost/exam.loc/wordpress/wp-content/uploads/2016/04/businessplus_logo.png"  alt="logo">
				<p>2015 © lawyer.</p>
			</div>
			<div class="col-lg-2">
				<h5>Navigation</h5>
				<?php wp_nav_menu( array( 'theme_location' => 'Footer-menu', 'container_class' => 'new_menu_class' ) ); ?>
			</div>
			<div class="col-lg-5">
				<h5>Quick contact us</h5>
				<?php echo do_shortcode('[contact-form-7 id="88" title="Contact form 1"]') ?>
			</div>
		</div>
	</footer><!-- #colophon -->


<?php wp_footer(); ?>

</body>
</html>
